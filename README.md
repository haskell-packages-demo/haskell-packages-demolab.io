# haskell-packages-demo.gitlab.io

# Debian packages
* [Packages overview for Debian Haskell Group
  ](https://qa.debian.org/developer.php?email=pkg-haskell-maintainers%40lists.alioth.debian.org)

# Parsing
* cat:[Parsing](https://hackage.haskell.org/packages/#cat:Parsing)

## conduit vs parsec
* [conduit vs parsec](https://google.com/search?q=conduit+vs+parsec)
  * [*How to compose streaming programs*
    ](https://www.tweag.io/posts/2017-10-05-streaming2.html)
    2017-10 Facundo Domínguez
  * [*Parsing*](https://guide.aelve.com/haskell/parsing-lnwybqv9) (2019)
  * [*attoparsec vs parsec, megaparsec, haskell csv parser example*
    ](http://northtexasroofing.net/article/31685685.shtml)
    (2019)

## URI parsing
* [haskell url parser](https://google.com/search?q=haskell+url+parser)
* [modern-uri](https://www.stackage.org/package/modern-uri)
* [network-uri](https://www.stackage.org/package/network-uri)
  * [Network.URI](https://www.stackage.org/haddock/nightly/network-uri/Network-URI.html)

## Markdown
* [mmark](https://www.stackage.org/package/mmark)

# Networking
* [network](https://hackage.haskell.org/package/network)

# HTTP clients
* [req](https://www.stackage.org/package/req)

# Hackage and Debian popularity
* ![Debian popularity 0](https://qa.debian.org/cgi-bin/popcon-png?packages=ghc+libghc-mtl-dev+libghc-text-dev+libghc-stm-dev+libghc-transformers-dev+libghc-xhtml-dev+libghc-terminfo-dev+libghc-haskeline-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
  * Included in `ghc` but not statistics available:
    * libghc-hpc-dev
    * libghc-integer-gmp-dev
    * libghc-libiserv-dev
    * libghc-parsec-dev
    * libghc-pretty-dev
    * libghc-process-dev
    * libghc-rts-dev
    * libghc-template-haskell-dev
    * libghc-time-dev
    * libghc-unix-dev
* ![Debian popularity 1](https://qa.debian.org/cgi-bin/popcon-png?packages=ghc+libghc-random-dev+libghc-vector-dev+libghc-attoparsec-dev+libghc-network-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 2](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-aeson-dev+libghc-free-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 2a](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-strict-dev+libghc-comonad-dev+libghc-blaze-html-dev+libghc-semigroupoids-dev+libghc-aeson-dev+libghc-conduit-dev+libghc-basement-dev+libghc-http-types-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2020-01-01&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 2b](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-free-dev+libghc-foundation-dev+libghc-operational-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2020-01-01&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 3](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-wai-dev+libghc-warp-dev+libghc-persistent-dev+libghc-yesod-core-dev+libghc-active-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 4a](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-active-dev+libghc-gi-gtk-dev+libghc-megaparsec-dev+libghc-rio-dev+libghc-postgresql-simple-dev+libghc-control-monad-free-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian popularity 4b](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-blaze-svg-dev+libghc-hakyll-dev+libghc-scotty-dev+libghc-control-monad-free-dev+libghc-operational-dev+libghc-shake-dev+libghc-esqueleto-dev+libghc-servant-dev+libghc-reactive-banana-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)


```shell
$ curl --silent -H 'Accept: application/json' https://hackage.haskell.org/packages/top
```


* 1530 aeson
  ![aeson](https://badgen.net/hackage/v/aeson)
  ![Debian package](https://img.shields.io/debian/v/libghc-aeson-dev)
* [880](https://hackage.haskell.org/package/warp) warp
* [787](https://hackage.haskell.org/package/persistent) persistent
* 785 network
* 774 yesod-core
* 700 megaparsec
* 665 lens (163 deb)
* 652 semigroupoids
* 644 conduit
* 602 shake (build system)
* 587 criterion
* 549 process
* 541 blaze-html
* 536 syb
* 521 stm
* 497 attoparsec (see: megaparsec)
* 488 basement
  * replaces: bytestring with foundation
* 486 vector (replaced by: foundation, see: basement)
* 483 sbv: SMT Based Verification: Symbolic Haskell theorem prover using SMT solving
* 466 reflection (165 deb)
* 457 free
* 448 JuicyPixels
* 438 parsec (see: megaparsec, attoparsec)
* 433 bytestring (see: basement, foundation)
* 418 random
* 403 kan-extensions (166 deb)
* 400 shakespeare (output templates)
* 393 servant
* 392 linear
* 382 snap (Web Framework)
* 343 esqueleto
* 328 Agda
* 327 comonad
* 322 hakyll
* 321 lucid (DSL for HTML (and SVG))
* 318 wai (see: servant, which may work on top of wai)
* 315 strict
* 309 yesod (see: servant ; but yesod may be easier; see also yesod-core)
* 304 strict-list
* 303 cairo
* 300 doctemplates (text templating from pandoc)
* 299 hpack
* 296 foundation
  * replaces: aeson, bytestring, cassava, conduit, Criterion, csv, dlist, json, network, QuickCheck, tasty, text, time, utf8-string, uuid, uuid-type, vector, xml, xml-conduit, yaml
  * [Status of replaced packages
    ](https://haskell-foundation.readthedocs.io/en/latest/porting/)
* 282 transformers
* 279 GLFW-b (windows with OpenGL contexts)
* 269 postgresql-simple (see: persistent)
* 257 comonad (736 deb)
* 253 strict-stm
* 252 http-types
* 247 mtl
* 234 distributive (737 deb)
* 228 active
* 227 hasql (see: persistent, postgresql-simple ; but hasql may be more CPU efficient)
* 223 haskell-gi
* 221 gloss
* 216 relude (see: foundation)
* 209 Chart
* 208 microstache (text templating, see: stache for better error messages)
* 203 diagrams-svg
* 200 active
* 194 text-rope
* 186 opaleye
* 184 scotty (see: servant or snap; but scotty may be easier than servant)
* 183 rio (see: relude or foundation)
* 181 text-builder
* 180 stack
* 179 heist (xml/html templating)
* 178 gi-gtk
* 176 bytestring-strict-builder
* 174 Spock (see: scotty)
* 168 GLUT
* 164 bytestring-builder
* 156 polysemy (free monads)
* 152 liquidhaskell (see: sbv)
* 145 flatparse (see: megaparsec)
* 137 ginger (Jinja2 template language)
* 132 ad (Automatic Differentiation)
* 131 polysemy
* 113 ghcjs-dom
* 126 plot
* 124 ede (templating language)
* 121 blaze-svg
* 114 operational (difficult monads)
* 110 array
* 108 reflex (FRP)
* 98 weigh (optimization)
* 96 first-class-families (type families)
* 91 yst (static websites)
* 90 fltkhs (see: gi-gtk)
* 81 reactive-banana
* 88 stache (text templating)
* 80 keter
* 79 not-gloss
* 78 gtk3 (see: gi-gtk)
* 76 dunai (see: reactive-banana)
* 72 fused-effects
* 70 type-of-html
* 68 text-builder-linear
* 68 text-ansi
* 67 wx (see: gi-gtk ; or Elixir language)
* 65 lucid-svg
* 63 spatial-math
* 62 selda (see: esqueleto)
* 58 control-monad-free
* 58 lucid2 (DSL for HTML)
* 52 Earley (see: megaparsec)
* 50 wumpus-core (PS and SVG generation)
* 46 rel8 (on top of: opaleye)
* 46 netwire (see: reactive-banana)
* 45 Rattus (see: reactive-banana)
* 40 stan
* 35 wai-handler-fastcgi
* 33 elerea (see: reactive-banana)
* 32 rope
* 32 happstack-lite (see: scotty)
* 29 yeshql (see: Clojure yesql)
* 28 simple (see: scotty)
* 24 fn (see: scotty)
* 20 reflex (see: reactive-banana)
* 19 shine (javascript graphics for the browser)
* 11 strict-wrapper
* 11 persistent-pagination
* 11 blaze-shields
